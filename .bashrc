#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
# alias ufetch="ufetch && sleep 8 && sleep 8d"
alias lsa="exa"
alias ls="exa -lai"
alias smci="sudo make clean install"
alias gt="git clone"
alias ..="cd .."
alias cl="clear"
alias rm="rm -i"
alias mv="mv -i"
alias vin="vim"
export EDITOR=vim
