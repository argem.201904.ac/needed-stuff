syntax on
set wildmode=longest,list,full
set number relativenumber
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
if &filetype ==# 'python'
	! python3 %
elseif &filetype ==# 'javascript'
	! node %	
elseif &filetype ==# 'c'
	! gcc %; ./a.out; rm a.out
elseif &filetype ==# 'cpp'
	! gcc %; ./a.out; rm a.out
endif
